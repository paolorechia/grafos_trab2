----------------------------------------------------
Trabalho 2 de Grafos - 2017-1
Turma B - Prof. Andr� Luiz Pires Guedes
Alunos: Luiz Gustavo Jhon Rodrigues - GRR 20153723
        Paolo Andreas Stall Rechia - GRR 20135196
----------------------------------------------------
Uso:
make
./conta_caminhos < grafo.dot
----------------------------------------------------
Descri��o:
O algoritmo consiste em iterar sobre todos os v�rtices do grafo para verificar se existe algum caminho a um n� com r�tulo,
partindo do n� atual. Cada itera��o consiste em colocar todos os filhos do n� em uma fila e verificar se ele tem um r�tulo. Caso
ele tenha um r�tulo, o valor do n� que estava sendo iterado � incrementado. Dessa forma o grafo � percorrido v�rias vezes e s�o 
descobertos caminhos at� os r�tulos para cada n�. Talvez haja uma implementa��o que percorra todo o grafo apenas uma vez, mas n�o 
descobrimos como implement�-la.
----------------------------------------------------

Pseudoc�digo

Para cada v�rtice 'v' do grafo:
    InicializaFila(fila)
    Enfileira 'v'
    Enquanto fila n�o estiver vazia:
        w <- Desenfileira
        Para cada arco de saida 'e' de 'w':
            u <- v�rtice de 'e' diferente de 'w'
            Enfileira 'u'
        Se 'w' diferente de 'v':
            Para cada atributo de 'w':
                Se atributo for igual a 1:
                    Incrementa atributo em 'v'
