#include <stdio.h>
#include <graphviz/cgraph.h>
#include <stdlib.h>
#include <string.h>

Agnode_t *fila[1000000];

int main(void) {
    Agraph_t *grafo = agread(stdin, NULL);
    Agnode_t *noAtual, *aux;
    int tam, i, attrAux, atAux;
    char strAux[30];

    //percorre todos os nos (lista de nos)
    //para cada no eh feita uma busca que percorre todos os caminhos possiveis
    for (Agnode_t *no = agfstnode(grafo); no; no = agnxtnode(grafo,no)){
        tam = 0;
        noAtual = no;
        fila[tam] = noAtual;
        tam++;
        //realiza esse loop ate a fila esvaziar
        while(tam != 0){         
            //primeiro da fila, colocamos seu filhos na fila
            aux = fila[0];
            for(Agedge_t *e = agfstout(grafo, aux); e; e = agnxtout(grafo,e)){
                //coloca o filho na fila
                fila[tam] = e->node;
                tam++;
            }
            tam--;
            //arrumos a fila pois o primeiro foi removido
            for(i = 0; i < tam; i++){
                fila[i] = fila[i+1];
            }
            if(noAtual != aux){
                for (Agsym_t *atributo = agnxtattr(grafo,AGNODE,NULL); atributo; atributo = agnxtattr(grafo,AGNODE,atributo)){
                    attrAux = atoi(agxget(aux, atributo));
                    if(attrAux == 1){
                        atAux = atoi(agxget(noAtual, atributo));
                        atAux++;
                        sprintf(strAux, "%d", atAux);
                        //o ultimo parametro é string
                        agset(noAtual, atributo->name, strAux);
                    }
                }
            }
        }
    }
    agwrite(grafo, stdout);
    return 0;
}
